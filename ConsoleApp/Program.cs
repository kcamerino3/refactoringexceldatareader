﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    class Program
    {
        private static string filePath;

        static void Main(string[] args)
        {
            filePath = "C:\\Users\\kimca\\Documents\\TestExcelReader.xlsx";

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {

                // Auto-detect format, supports:
                //  - Binary Excel files (2.0-2003 format; *.xls)
                //  - OpenXml Excel files (2007 format; *.xlsx)
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {

                    // Choose one of either 1 or 2:

                    // 1. Use the reader methods
                    do
                    {
                        int columnnumber = 0;

                        while (reader.Read())
                        {
                            while (columnnumber <= reader.FieldCount)
                            {
                                var value = reader.GetValue(columnnumber);
                                Console.WriteLine(value);
                                columnnumber++;
                                continue;
                            }


                        }

                    } while (reader.NextResult());

                    // 2. Use the AsDataSet extension method
                    //var result = reader.AsDataSet();
                    

                    // The result of each spreadsheet is in result.Tables
                }
            }
        }
    }
}
//public static bool ParseReference(string value, out int column, out int row)
//{
//    column = 0;
//    var position = 0;
//    const int offset = 'A' - 1;

//    if (value != null)
//    {
//        while (position < value.Length)
//        {
//            var c = value[position];
//            if (c >= 'A' && c <= 'Z')
//            {
//                position++;
//                column *= 26;
//                column += c - offset;
//                continue;
//            }

//            if (char.IsDigit(c))
//                break;

//            position = 0;
//            break;
//        }
//    }

//    if (position == 0)
//    {
//        column = 0;
//        row = 0;
//        return false;
//    }

//    if (!int.TryParse(value.Substring(position), NumberStyles.None, CultureInfo.InvariantCulture, out row))
//    {
//        return false;
//    }

//    return true;
//}
//    }
//}
﻿using System.Globalization;

namespace ExcelDataReader.Core
{
    internal class ReferenceHelper
    {
        //static int col = 0;
        //static int position = 0;

        /// <summary>
        /// Logic for the Excel dimensions. Ex: A15
        /// </summary>
        /// <param name="cellAddress">The address of the Excel cell (Column/Row) Ex: A15.</param>
        /// <param name="column">The Excel column, 1-based.</param>
        /// <param name="row">The Excel row, 1-based.</param>

        public static bool ParseReference(string cellAddress, out int column, out int row)
        {
            column = 0;
            var position = 0;
            const int offset = 'A' - 1;

            if (cellAddress != null)
            {
                while (position < cellAddress.Length)
                {
                    var cellAddressCharAtPostion = cellAddress[position];
                    if (cellAddressCharAtPostion >= 'A' && cellAddressCharAtPostion <= 'Z')
                    {
                        position++;
                        column *= 26;
                        column += cellAddressCharAtPostion - offset;
                        continue;
                    }

                    if (char.IsDigit(cellAddressCharAtPostion))
                        break;

                    position = 0;
                    break;
                }
            }

            Test(position, cellAddress);
        }


        public static bool Test(int position, string cellAddress)
        {
            if (position == 0)
            {
                column = 0;
                row = 0;
                return false;
            }

            if (!int.TryParse(cellAddress.Substring(position), NumberStyles.None, CultureInfo.InvariantCulture, out row))
            {
                return false;
            }

            return true;
        }
    }
}
//        public static bool ParseReference(string value, out int column, out int row)
//        {
//            column = 0;
//            var position = 0;
//            const int offset = 'A' - 1;

//            if (value != null)
//            {
//                while (position < value.Length)
//                {
//                    var c = value[position];
//                    if (c >= 'A' && c <= 'Z')
//                    {
//                        position++;
//                        column *= 26;
//                        column += c - offset;
//                        continue;
//                    }

//                    if (char.IsDigit(c))
//                        break;

//                    position = 0;
//                    break;
//                }
//            }

//            if (position == 0)
//            {
//                column = 0;
//                row = 0;
//                return false;
//            }

//            if (!int.TryParse(value.Substring(position), NumberStyles.None, CultureInfo.InvariantCulture, out row))
//            {
//                return false;
//            }

//            return true;
//        }
//    }
//}